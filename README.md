# Anti-CD40 analysis #

This repository is for the RNA-Seq, 16S rRNA gene sequencing, bile acid, blood cytokine and FACs data from the Blake et al. "The immunotoxicity, but not anti-tumor efficacy, of anti-CD40 and anti-CD137 immunotherapies is dependent on the gut microbiota on anti-CD40 treatment." published in Cell Reports Medicine.