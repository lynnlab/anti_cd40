#!/usr/bin/env python
# coding: utf-8

import h5py
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib_scalebar.scalebar import ScaleBar
from scipy import stats


# ## Load data as HDF5

hdf = h5py.File('mass_spec_db.h5', 'r')
dimensions = {
    'No ABX Treatment': (108, 119),
    'ABX Treated': (106, 115),
    'GF': (96, 123),
}


# ## Apply mean intensity fold change threshold

# ## Load spectrum index

ind = hdf['spectrum'][()]

matches = {}
idx = 0
for i, mass in enumerate(ind):
    
    # we want the really big peaks
    vmax = np.max([ np.percentile(hdf[name][i],95) for name in dimensions.keys() ])
    if vmax < 4000:
        continue
    
    # sometimes there are lots of zeros, discard
    nzero = np.max([ np.sum(hdf[name][i] == 0) for name in dimensions.keys() ])
    if nzero > 100:
        continue
        
    no_abx_mean = hdf['No ABX Treatment'][i].mean()
    gf_mean = hdf['GF'][i].mean()
    abx_mean = hdf['ABX Treated'][i].mean()
    fc_abx = np.log2(abx_mean / no_abx_mean)
    fc_gf = np.log2(gf_mean / no_abx_mean)
    if ((np.abs(fc_abx)) > 0.5) or ((np.abs(fc_gf)) > 0.5):
        matches[idx] = i
        idx += 1


# ## Define a custom colourmap

lsc = [
    (0.0, (0, 0, 0, 255)), 
    (0.232886, (32, 0, 129, 255)),
    (0.525758, (115, 15, 255, 255)),
    (0.836273, (255, 255, 0, 255)),
    (1.0, (255, 255, 255, 255))
]

cdict = {'red':   [ (i[0], i[1][0]/255, i[1][0]/255) for i in lsc ],
         'green': [ (i[0], i[1][1]/255, i[1][1]/255) for i in lsc ],
         'blue':  [ (i[0], i[1][2]/255, i[1][2]/255) for i in lsc ]}

ccmap = mpl.colors.LinearSegmentedColormap('yellowy', segmentdata=cdict, N=256)


# ## Names of potential lipid matches obtained from LIPID MAPS Structural Database

name_map = {
    "255.234": "palmitic acid",
    "281.247": "oleic acid",
    "283.261": "stearic acid",
    "284.265": "C17 sphingosine",
    "303.233": "arachnidonic acid",
    "327.236": "DHA",
    "465.306": "cholesterol sulfate",
    "514.285": "taurallocholic acid",
}


# ## Define plotting function
# 
# This is rather complex as it produces a multi-panel figure with spectrum histogram.

def plot(pick, cmap):
    
    # Setup figure
    fig = plt.figure(constrained_layout=False, figsize=(15,5))
    gs = fig.add_gridspec(1,4, width_ratios=(1,1,1,.2))
    axes = [fig.add_subplot(gs[0,i]) for i in range(0,4)]
    colour_ax = axes[-1]
    hist_ax = colour_ax.twiny()
    
    
    # Determine max intensity threshold
    vmax = np.max([ np.percentile(hdf[name][pick],95) for name in dimensions.keys() ])
    vrealmax = np.max([ np.percentile(hdf[name][pick],99.5) for name in dimensions.keys() ])
    
    # Plot each histogram
    for (name, dimension) in dimensions.items():
        spec = hdf[name][pick]
        density = stats.kde.gaussian_kde(spec.flatten())
        x = np.linspace(spec.flatten().min(), vrealmax, 200)
        hist_ax.fill(np.append(density(x), 0),
            np.append(x, vrealmax+1), alpha=0.8, label=name)
    hist_ax.set_xticks([])
    hist_ax.set_xlim(0)
    hist_ax.legend(bbox_to_anchor=(-0.2, 1.02, 1., .102))
    
    
    # Plot each liver section
    nf = mpl.colors.Normalize(vmin=0, vmax=vmax)
    n = 0
    for (name, dimension), ax in zip(dimensions.items(), axes):
        spec = hdf[name][pick]+1

        im = ax.pcolormesh(np.flipud(spec), rasterized=True, cmap=cmap, norm=nf)
        for spine in ax.spines.values():
            spine.set_edgecolor(mpl.cm.tab10(n))
            spine.set_linewidth(4)
        n+=1
        
        ax.set_title(name, fontsize=13)
        ax.set_xticks([])
        ax.set_yticks([])

        scalebar = ScaleBar(0.02, 'mm', fixed_value=0.2, border_pad=1.0, box_alpha=0.7, location='lower left')
        ax.add_artist(scalebar)
    
    cb = mpl.colorbar.ColorbarBase(colour_ax, orientation='vertical',
        ticks=mpl.ticker.AutoLocator(), cmap=cmap, norm=nf, values=np.linspace(0,vrealmax,256))
    cb.set_label('Intensity')
    hist_ax.invert_xaxis()

    # Add mass annotation if it exists
    if f'{ind[pick]:.3f}' in name_map.keys():
        mass_string = f'{ind[pick]:.3f}'
        fig.suptitle(f'{mass_string} m/z ({name_map[mass_string]})', fontsize=20, y=1.01)
    else:
        fig.suptitle(f'{ind[pick]:.3f} m/z', fontsize=20, y=1.01)
    return fig


# ## Plot all desired items and save as pdf

for match, item, in matches.items():
    # Custom exclusion of certain matches
    if match in [0,1,2,3,4,5,6,18,19,20]:
        continue
    fig = plot(item, cmap=ccmap)
    fig.savefig(f'{ind[item]:.3f}_plot.pdf', dpi=150, bbox_inches='tight')
    
hdf.close()

