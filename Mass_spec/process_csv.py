#!/usr/bin/env python
# coding: utf-8

# # Process and clean CSV files
# 
# This notebook converts the mass spec csv files into a more easily loadable HDF5 database.

import glob, re, h5py
import numpy as np
import pandas as pd


# ## Load csv files

data_dir = 'data'
spectra  = {}

for file in glob.glob(data_dir+'/*'):
    pattern = data_dir + '[/](.*).csv'
    name = re.search(pattern, file).group(1)
    spectra[name] = pd.read_csv(file, sep=';', skiprows=10, index_col=0)


# ## Minimal data cleaning

# Basic threshold to exclude extremely low intensity peaks
thres = 100 
keep = ~(
    (spectra['GF'].mean() < thres) &
    (spectra['No ABX Treatment'].mean() < thres) &
    (spectra['ABX Treated'].mean() < thres)
)
# There appear to be duplicate rows (with a ".1" suffix)
# I'll exclude them here
keep = keep & ~keep.index.str.endswith('.1')


# ## Specify x,y dimensions of each dataset

dimensions = {
    'No ABX Treatment': (108, 119),
    'ABX Treated': (106, 115),
    'GF': (96, 123),
}


# ## Convert to HDF5 format
# 
# This is simply to make the data faster to load and work with.

hdf = h5py.File('mass_spec_db.h5', 'w')

for file in dimensions.keys():
    lx, ly = dimensions[file]
    spec = spectra[file].loc[:, keep]
    lz = spec.shape[1]
    z = np.zeros((lx, ly, lz))
    i = 0
    for y in range(0, ly):
        for x in range(0, lx):
            if i >= len(spec):
                break
            z[x, y] = spec.iloc[i]
            i += 1
    hdf.create_dataset(file, data=z.T.astype(int), dtype='i4') # 32 bit integers


# ## Also create an index for m/z spectrum values

hdf.create_dataset('spectrum', data=spectra['GF'].loc[:, keep].columns.astype(float).values, dtype='f4')

hdf.close()

